package com.bitconex.tc.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Swagger3Config {

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("public")
                .pathsToMatch("/**").packagesToScan("com.bitconex.tc.controller")
                .build();
    }

    @Bean
    public OpenAPI springShopOpenAPI() {
        var info = new Info().title("Tourist Community API").version("v1");

//        String securitySchemeName = "bearerAuth";
//        var securityScheme = new SecurityScheme().name(securitySchemeName).type(SecurityScheme.Type.HTTP)
//                .scheme("bearer").bearerFormat("JWT");
//        var securityComponents = new Components().addSecuritySchemes(securitySchemeName, securityScheme);
        return new OpenAPI()
                .addServersItem(
                        new Server().url("/")
                )
                .info(info);
//                .addSecurityItem(
//                        new SecurityRequirement().addList(securitySchemeName)
//                )
//                .components(securityComponents);
    }

}
