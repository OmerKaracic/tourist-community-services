package com.bitconex.tc.service;

import com.bitconex.tc.exception.NotFoundException;
import com.bitconex.tc.model.Country;
import com.bitconex.tc.payload.CountryPayload;
import com.bitconex.tc.repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CountryService {

    @Autowired
    CountryRepository countryRepository;

    public List<Country> findAll() {
        log.info("Finding all countries...");
        return countryRepository.findAll();
    }

    public Country findById(Integer id) {
        log.info("Finding country by ID :: {}", id);
        Optional<Country> countryOptional = countryRepository.findById(id);

        if (countryOptional.isEmpty()) throw new NotFoundException("Country not found");

        return countryOptional.get();
    }

    public Country save(CountryPayload countryPayload) {
        log.info("Saving new country :: {} - {}", countryPayload.getCode(), countryPayload.getName());
        return countryRepository.save(
                new Country(
                        countryPayload.getName(),
                        countryPayload.getCode()
                )
        );
    }

    public Country update(Integer id, CountryPayload countryPayload) {
        log.info("Updating country by ID :: {}", id);
        Optional<Country> countryOptional = countryRepository.findById(id);

        if (countryOptional.isEmpty()) throw new NotFoundException("Country not found");

        Country country = countryOptional.get();
        country.setName(countryPayload.getName());
        country.setCode(countryPayload.getCode());

        return countryRepository.save(country);
    }

    public void delete(Integer id) {
        log.info("Deleting country by ID :: {}", id);
        Optional<Country> countryOptional = countryRepository.findById(id);

        if (countryOptional.isEmpty()) throw new NotFoundException("Country not found");
//        if (countryOptional.get().getMunicipalities().size() > 0) throw new RuntimeException("Delete municipalities before deleting country.");

        countryRepository.delete(countryOptional.get());
    }
}
