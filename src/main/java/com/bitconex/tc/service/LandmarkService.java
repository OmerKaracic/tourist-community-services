package com.bitconex.tc.service;

import com.bitconex.tc.exception.NotFoundException;
import com.bitconex.tc.model.*;
import com.bitconex.tc.payload.LandmarkPayload;
import com.bitconex.tc.payload.ReviewPayload;
import com.bitconex.tc.repository.LandmarkRepository;

import com.bitconex.tc.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LandmarkService {

    @Autowired
    LandmarkRepository landmarkRepository;

    @Autowired
    CoordinatesService coordinatesService;

    @Autowired
    ImportanceService importanceService;

    @Autowired
    MunicipalityService municipalityService;

    @Autowired
    ReviewTypeService reviewTypeService;

    @Autowired
    ReviewRepository reviewRepository;


    public List<Landmark> findAll() {
        log.info("Finding all landmarks...");
        return landmarkRepository.findAll();
    }

    public Landmark findById(Long id) {
        log.info("Finding landmark by ID :: {}", id);
        Optional<Landmark> landmarkOptional = landmarkRepository.findById(id);

        if (landmarkOptional.isEmpty()) throw new NotFoundException("Landmark not found");

        return landmarkOptional.get();
    }

    public List<Landmark> findAllByMunicipality(Long municipalityId) {
        log.info("Finding all landmarks by municipality ID :: {}", municipalityId);
        return landmarkRepository.findAllByMunicipality_Id(municipalityId);
    }

    public Landmark save(LandmarkPayload landmarkPayload) {
        log.info("Saving new landmark :: {}", landmarkPayload.getName());
        Coordinates coordinates = coordinatesService.save(landmarkPayload.getCoordinates());
        Importance importance = importanceService.findById(landmarkPayload.getImportanceId());
        Municipality municipality = municipalityService.findById(landmarkPayload.getMunicipalityId());
        return landmarkRepository.save(
                new Landmark(
                        landmarkPayload.getName(),
                        landmarkPayload.getDescription(),
                        coordinates,
                        importance,
                        municipality
                )
        );
    }

    public Landmark update(Long id, LandmarkPayload landmarkPayload) {
        log.info("Updating landmark by ID :: {}", id);
        Optional<Landmark> landmarkOptional = landmarkRepository.findById(id);

        if (landmarkOptional.isEmpty()) throw new NotFoundException("Landmark not found");

        Landmark landmark = landmarkOptional.get();
        landmark.setName(landmarkPayload.getName());
        landmark.setDescription(landmarkPayload.getDescription());

        return landmarkRepository.save(landmark);
    }

    public void delete(Long id) {
        log.info("Deleting landmark by ID :: {}", id);
        Optional<Landmark> landmarkOptional = landmarkRepository.findById(id);

        if (landmarkOptional.isEmpty()) throw new NotFoundException("Landmark not found");

        landmarkRepository.delete(landmarkOptional.get());
    }

    public Review review(Long landmarkId, ReviewPayload reviewPayload) {
        log.info("Adding review to landmark ID :: {}", landmarkId);
        ReviewType reviewType = reviewTypeService.findById(reviewPayload.getReviewTypeId());
        Landmark landmark = findById(landmarkId);
        Review review = reviewRepository.save(
                new Review(
                        landmark,
                        reviewType
                )
        );
        landmark.getReviews().add(review);
        landmarkRepository.save(landmark);

        return review;
    }

    public List<Landmark> search(String name, String importanceName) {
        log.info("Searching active landmarks by name :: {} | importance :: {}", name, importanceName);
        List<Landmark> landmarks;

        if (name == null && importanceName == null) throw new RuntimeException("Search params are not presented"); else
        if (name != null && importanceName != null) {
            Importance importance = importanceService.findByName(importanceName);
            landmarks = landmarkRepository.findAllByNameContainingAndImportance_IdAndActive(name, importance.getId(), true);
        } else if (name != null) {
            landmarks = landmarkRepository.findAllByNameContainingAndActive(name, true);
        } else {
            Importance importance = importanceService.findByName(importanceName);
            landmarks = landmarkRepository.findAllByImportance_IdAndActive(importance.getId(), true);
        }
        return landmarks;
    }

    public Landmark toggleActive(Long landmarkId, boolean isActive) {
        log.info("Toggling activation for landmark ID :: {}", landmarkId);
        Landmark landmark = findById(landmarkId);
        landmark.setActive(isActive);

        return landmarkRepository.save(landmark);
    }
}
