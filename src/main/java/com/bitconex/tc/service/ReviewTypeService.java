package com.bitconex.tc.service;

import com.bitconex.tc.exception.NotFoundException;
import com.bitconex.tc.model.ReviewType;
import com.bitconex.tc.payload.ReviewTypePayload;
import com.bitconex.tc.repository.ReviewTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class ReviewTypeService {

    @Autowired
    ReviewTypeRepository reviewTypeRepository;

    public boolean existsByName(String name) {
        log.info("Checking if review exists :: {}", name);
        return reviewTypeRepository.existsByName(name);
    }

    public ReviewType save(ReviewTypePayload review) {
        log.info("Saving new review :: {}", review.getName());
        return reviewTypeRepository.save(
                new ReviewType(
                        review.getName(),
                        review.getRating()
                )
        );
    }

    public ReviewType findById(Integer reviewTypeId) {
        log.info("Finding review type by ID :: {}", reviewTypeId);
        Optional<ReviewType> reviewType = reviewTypeRepository.findById(reviewTypeId);

        if (reviewType.isEmpty()) throw new NotFoundException("Review type not found");

        return reviewType.get();
    }
}
