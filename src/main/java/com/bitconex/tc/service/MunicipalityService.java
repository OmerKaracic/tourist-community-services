package com.bitconex.tc.service;

import com.bitconex.tc.exception.NotFoundException;
import com.bitconex.tc.model.Country;
import com.bitconex.tc.model.Municipality;
import com.bitconex.tc.payload.MunicipalityPayload;
import com.bitconex.tc.repository.MunicipalityRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MunicipalityService {

    @Autowired
    MunicipalityRepository municipalityRepository;

    @Autowired
    CountryService countryService;

    public Municipality findById(Long id) {
        log.info("Finding municipality by ID :: {}", id);
        Optional<Municipality> municipalityOptional = municipalityRepository.findById(id);

        if (municipalityOptional.isEmpty()) throw new NotFoundException("Municipality not found");

        return municipalityOptional.get();
    }

    public List<Municipality> findAllByCountry(Integer countryId) {
        log.info("Finding all municipalities by country ID :: {}", countryId);
        Country country = countryService.findById(countryId);
        return municipalityRepository.findAllByCountry_Id(country.getId());
    }

    public Municipality save(MunicipalityPayload municipalityPayload) throws NotFoundException {
        log.info("Saving new municipality :: {}", municipalityPayload.getName());
        Country country = countryService.findById(municipalityPayload.getCountryId());
        return municipalityRepository.save(
                new Municipality(
                        municipalityPayload.getName(),
                        country
                )
        );
    }

    public Municipality update(Long municipalityId, MunicipalityPayload municipalityPayload) {
        log.info("Updating municipality by ID :: {}", municipalityId);
        Optional<Municipality> municipalityOptional = municipalityRepository.findById(municipalityId);

        if (municipalityOptional.isEmpty()) throw new NotFoundException("Municipality not found");

        Municipality municipality = municipalityOptional.get();
        municipality.setName(municipalityPayload.getName());

        Country country = countryService.findById(municipalityPayload.getCountryId());
        municipality.setCountry(country);

        return municipalityRepository.save(municipality);
    }

    public void delete(Long id) {
        log.info("Deleting municipality by ID : {}", id);
        Optional<Municipality> municipalityOptional = municipalityRepository.findById(id);

        if (municipalityOptional.isEmpty()) throw new NotFoundException("Municipality not found");

        municipalityRepository.delete(municipalityOptional.get());
    }
}
