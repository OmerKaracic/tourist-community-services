package com.bitconex.tc.service;

import com.bitconex.tc.exception.NotFoundException;
import com.bitconex.tc.model.Importance;
import com.bitconex.tc.payload.ImportancePayload;
import com.bitconex.tc.repository.ImportanceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ImportanceService {

    @Autowired
    ImportanceRepository importanceRepository;


    public boolean existsByName(String name) {
        return importanceRepository.existsByName(name);
    }

    public List<Importance> findAll() {
        log.info("Finding all importance types...");
        return importanceRepository.findAll();
    }

    public Importance findById(Integer id) {
        log.info("Finding importance type by ID :: {}", id);
        Optional<Importance> importanceOptional = importanceRepository.findById(id);

        if (importanceOptional.isEmpty()) throw new NotFoundException("Importance not found.");

        return importanceOptional.get();
    }

    public Importance findByName(String importanceName) {
        log.info("Finding importance type by name :: {}", importanceName);
        Optional<Importance> importanceOptional = importanceRepository.findByName(importanceName);

        if (importanceOptional.isEmpty()) throw new NotFoundException("Importance not found.");

        return importanceOptional.get();
    }

    public Importance save(ImportancePayload importancePayload) {
        log.info("Saving new importance type :: {}", importancePayload.getName());
        return importanceRepository.save(
                new Importance(
                        importancePayload.getName()
                )
        );
    }
}
