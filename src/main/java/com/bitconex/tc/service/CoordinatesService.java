package com.bitconex.tc.service;

import com.bitconex.tc.model.Coordinates;
import com.bitconex.tc.payload.CoordinatesPayload;
import com.bitconex.tc.repository.CoordinatesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CoordinatesService {

    @Autowired
    CoordinatesRepository coordinatesRepository;

    public Coordinates save(CoordinatesPayload coordinatesPayload) {
        log.info("Saving new coordinates :: {} - {}", coordinatesPayload.getLat(), coordinatesPayload.getLng());
        return coordinatesRepository.save(
                new Coordinates(
                        coordinatesPayload.getLat(),
                        coordinatesPayload.getLng()
                )
        );
    }
}
