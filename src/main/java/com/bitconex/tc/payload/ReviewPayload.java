package com.bitconex.tc.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReviewPayload implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer reviewTypeId;
}
