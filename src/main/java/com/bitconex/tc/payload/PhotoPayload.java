package com.bitconex.tc.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PhotoPayload implements Serializable {

    private static final long serialVersionUID = 1L;

    private String path;
    private Long landmarkId;
}
