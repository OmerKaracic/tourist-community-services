package com.bitconex.tc.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor @AllArgsConstructor
public class LandmarkPayload implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String description;
    private CoordinatesPayload coordinates;
    private Integer importanceId;
    private Long municipalityId;
    private boolean active;
}
