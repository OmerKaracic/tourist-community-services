package com.bitconex.tc.repository;

import com.bitconex.tc.model.Municipality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MunicipalityRepository extends JpaRepository<Municipality, Long> {
    List<Municipality> findAllByCountry_Id(Integer countryId);
}
