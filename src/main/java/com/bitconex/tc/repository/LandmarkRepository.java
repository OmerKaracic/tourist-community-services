package com.bitconex.tc.repository;

import com.bitconex.tc.model.Landmark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LandmarkRepository extends JpaRepository<Landmark, Long> {
    List<Landmark> findAllByMunicipality_Id(Long municipalityId);

    List<Landmark> findAllByNameContainingAndImportance_IdAndActive(String name, Integer id, boolean active);

    List<Landmark> findAllByNameContainingAndActive(String name, boolean active);

    List<Landmark> findAllByImportance_IdAndActive(Integer id, boolean active);
}
