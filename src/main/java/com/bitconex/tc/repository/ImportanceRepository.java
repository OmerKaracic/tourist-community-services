package com.bitconex.tc.repository;

import com.bitconex.tc.model.Importance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImportanceRepository extends JpaRepository<Importance, Integer> {

    boolean existsByName(String name);

    Optional<Importance> findByName(String importanceName);
}
