package com.bitconex.tc.repository;

import com.bitconex.tc.model.ReviewType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewTypeRepository extends JpaRepository<ReviewType, Integer> {

    boolean existsByName(String name);
}
