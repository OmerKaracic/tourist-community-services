package com.bitconex.tc;

import com.bitconex.tc.payload.ImportancePayload;
import com.bitconex.tc.payload.ReviewTypePayload;
import com.bitconex.tc.service.ImportanceService;
import com.bitconex.tc.service.ReviewTypeService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.stream.Stream;

@SpringBootApplication
public class TouristCommunityApplication {

	public static void main(String[] args) {
		SpringApplication.run(TouristCommunityApplication.class, args);
	}

	@Bean
	CommandLineRunner start(ImportanceService importanceService, ReviewTypeService reviewTypeService) {
		return args-> {
			Stream.of("MEMORABLE", "VERY_MEMORABLE", "UNAVOIDABLY").forEach( u -> {
				if (!importanceService.existsByName(u))
			 		importanceService.save(new ImportancePayload(u));
			});

			ReviewTypePayload[] reviewTypes = {
					new ReviewTypePayload("BAD", 1),
					new ReviewTypePayload("NOT_BAD", 2),
					new ReviewTypePayload("GOOD", 3),
					new ReviewTypePayload("VERY_GOOD", 4),
					new ReviewTypePayload("EXCELLENT", 5)
			};

			Arrays.stream(reviewTypes).forEach(review -> {
				if (!reviewTypeService.existsByName(review.getName()))
					reviewTypeService.save(review);
			});
		};
	}

}
