package com.bitconex.tc.controller;

import com.bitconex.tc.model.Landmark;
import com.bitconex.tc.model.Review;
import com.bitconex.tc.payload.LandmarkPayload;
import com.bitconex.tc.payload.ReviewPayload;
import com.bitconex.tc.service.LandmarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/landmark")
public class LandmarkController {

    @Autowired
    LandmarkService landmarkService;


    @GetMapping()
    public ResponseEntity<List<Landmark>> getLandmarks() {
        return new ResponseEntity<>(landmarkService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{landmarkId}")
    public ResponseEntity<Landmark> getLandmarkById(@PathVariable Long landmarkId) {
        return new ResponseEntity<>(landmarkService.findById(landmarkId), HttpStatus.OK);
    }

    @GetMapping("/municipality/{municipalityId}")
    public ResponseEntity<List<Landmark>> getLandmarksByMunicipality(@PathVariable Long municipalityId) {
        return new ResponseEntity<>(landmarkService.findAllByMunicipality(municipalityId), HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<List<Landmark>> searchLandmarks(@RequestParam(name = "n", required = false) String name, @RequestParam(name = "i", required = false) String importance) {
        return new ResponseEntity<>(landmarkService.search(name, importance), HttpStatus.OK);
    }

    @PostMapping("/{landmarkId}/review")
    public ResponseEntity<Review> reviewLandmark(@PathVariable Long landmarkId, @RequestBody ReviewPayload reviewPayload) {
        return new ResponseEntity<>(landmarkService.review(landmarkId, reviewPayload), HttpStatus.CREATED);
    }

    @PostMapping
    public ResponseEntity<Landmark> saveLandmark(@RequestBody LandmarkPayload landmarkPayload) {
        return new ResponseEntity<>(landmarkService.save(landmarkPayload), HttpStatus.CREATED);
    }

    @PutMapping("/{landmarkId}")
    public ResponseEntity<Landmark> updateLandmark(@PathVariable Long landmarkId, @RequestBody LandmarkPayload landmarkPayload) {
        return new ResponseEntity<>(landmarkService.update(landmarkId, landmarkPayload), HttpStatus.OK);
    }

    @PutMapping("/{landmarkId}/{isActive}")
    public ResponseEntity<Landmark> toggleActive(@PathVariable Long landmarkId, @PathVariable boolean isActive) {
        return new ResponseEntity<>(landmarkService.toggleActive(landmarkId, isActive), HttpStatus.OK);
    }

    @DeleteMapping("/{landmarkId}")
    public ResponseEntity<?> delete(@PathVariable Long landmarkId) {
        landmarkService.delete(landmarkId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
