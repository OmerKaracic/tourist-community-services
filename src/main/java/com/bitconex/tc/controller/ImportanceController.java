package com.bitconex.tc.controller;

import com.bitconex.tc.model.Importance;
import com.bitconex.tc.service.ImportanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/importance/type")
public class ImportanceController {

    @Autowired
    ImportanceService importanceService;
    
    @GetMapping()
    public ResponseEntity<List<Importance>> getImportanceTypes() {
        return new ResponseEntity<>(importanceService.findAll(), HttpStatus.OK);
    }
}
