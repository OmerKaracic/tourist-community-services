package com.bitconex.tc.controller;

import com.bitconex.tc.model.Country;
import com.bitconex.tc.payload.CountryPayload;
import com.bitconex.tc.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/country")
public class CountryController {

    @Autowired
    CountryService countryService;

    @GetMapping
    public ResponseEntity<List<Country>> getCountries() {
        return new ResponseEntity<>(countryService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{countryId}")
    public ResponseEntity<Country> getCountryById(@PathVariable Integer countryId) {
        return new ResponseEntity<>(countryService.findById(countryId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Country> saveCountry(@RequestBody CountryPayload countryPayload) {
        return new ResponseEntity<>(countryService.save(countryPayload), HttpStatus.CREATED);
    }

    @PutMapping("/{countryId}")
    public ResponseEntity<Country> updateCountry(@PathVariable Integer countryId, @RequestBody CountryPayload countryPayload) {
        return new ResponseEntity<>(countryService.update(countryId, countryPayload), HttpStatus.OK);
    }

    @DeleteMapping("{countryId}")
    public ResponseEntity<?> deleteCountry(@PathVariable Integer countryId) {
        countryService.delete(countryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
