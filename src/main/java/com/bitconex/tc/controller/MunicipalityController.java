package com.bitconex.tc.controller;

import com.bitconex.tc.model.Municipality;
import com.bitconex.tc.payload.MunicipalityPayload;
import com.bitconex.tc.service.MunicipalityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/municipality")
public class MunicipalityController {
    
    @Autowired
    MunicipalityService municipalityService;

    @GetMapping("/{municipalityId}")
    public ResponseEntity<Municipality> getMunicipalityById(@PathVariable Long municipalityId) {
        return new ResponseEntity<>(municipalityService.findById(municipalityId), HttpStatus.OK);
    }

    @GetMapping("/country/{countryId}")
    public ResponseEntity<List<Municipality>> getMunicipalitiesByCountry(@PathVariable Integer countryId) {
        return new ResponseEntity<>(municipalityService.findAllByCountry(countryId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Municipality> saveMunicipality(@RequestBody MunicipalityPayload municipalityPayload) {
        return new ResponseEntity<>(municipalityService.save(municipalityPayload), HttpStatus.CREATED);
    }

    @PutMapping("/{municipalityId}")
    public ResponseEntity<Municipality> updateMunicipality(@PathVariable Long municipalityId, @RequestBody MunicipalityPayload municipalityPayload) {
        return new ResponseEntity<>(municipalityService.update(municipalityId, municipalityPayload), HttpStatus.OK);
    }

    @DeleteMapping("{municipalityId}")
    public ResponseEntity<?> deleteMunicipality(@PathVariable Long municipalityId) {
        municipalityService.delete(municipalityId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
