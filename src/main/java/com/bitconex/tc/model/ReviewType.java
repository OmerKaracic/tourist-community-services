package com.bitconex.tc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "review_types")
public class ReviewType {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Integer rating;

    public ReviewType(String name, Integer rating) {
        this.name = name;
        this.rating = rating;
    }
}
