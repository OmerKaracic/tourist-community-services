package com.bitconex.tc.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "landmarks")
public class Landmark {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String description;

    @JsonManagedReference
    @OneToMany
    private List<Photo> photos = new ArrayList<>();

    @OneToOne
    private Coordinates coordinates;

    @OneToOne
    private Importance importance;

    @JsonManagedReference
    @OneToMany
    private List<Review> reviews = new ArrayList<>();

    @JsonManagedReference
    @ManyToOne
    private Municipality municipality;

    private boolean active;

    public Landmark(String name, String description, Coordinates coordinates, Importance importance, Municipality municipality) {
        this.name = name;
        this.description = description;
        this.coordinates = coordinates;
        this.importance = importance;
        this.municipality = municipality;
        this.active = true;
    }
}
