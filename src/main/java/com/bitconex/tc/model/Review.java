package com.bitconex.tc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "reviews")
public class Review {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonBackReference
    @ManyToOne
    private Landmark landmark;

    @JsonManagedReference
    @ManyToOne
    private ReviewType reviewType;

    public Review(Landmark landmark, ReviewType reviewType) {
        this.landmark = landmark;
        this.reviewType = reviewType;
    }
}
