package com.bitconex.tc.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "municipalities")
public class Municipality {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @JsonManagedReference
    @ManyToOne
    private Country country;

    @JsonBackReference
    @OneToMany
    private List<Landmark> landmarks = new ArrayList<>();

    public Municipality(String name, Country country) {
        this.name = name;
        this.country = country;
    }
}
